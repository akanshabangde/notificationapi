package com.application.appRunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.application")
public class ApplicationRunner {
	
	public static void main(String[] args) {
	SpringApplication.run(ApplicationRunner.class, args);	
	}

}
