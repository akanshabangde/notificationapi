package com.application.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.text.ParseException;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.boot.json.JsonParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MindController {
	
	@RequestMapping(value = "/mindSphereEventAPI", method = RequestMethod.GET)
	public ResponseEntity<?> mindSphereAPI() throws Exception {
		
		String url = "https://gateway.eu1.mindsphere.io/api/iottimeseries/v3/timeseries/ce5918bc75ad48fd85e119a1afcf060d/dtfe48?from=2019-02-07T10:00:51.755Z&to=2019-02-12T17:00:51.755Z&limit=100";
		Proxy proxy=null;
		URL urlObj;
		HttpsURLConnection connection = null;
		try {
			urlObj = new URL(url);
			if(proxy==null){
				connection = (HttpsURLConnection) urlObj.openConnection();
			}else{

				connection = (HttpsURLConnection) urlObj.openConnection(proxy);
			}
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", "Bearer "+"eyJhbGciOiJSUzI1NiIsImprdSI6Imh0dHBzOi8vYXRvc2VkZS5sb2NhbGhvc3Q6ODA4MC91YWEvdG9rZW5fa2V5cyIsImtpZCI6ImtleS1pZC0xIiwidHlwIjoiSldUIn0.eyJqdGkiOiIwMzU0ODg2MzhhOTM0MjVlYjVjZGFhYzM3ZWFlMWEyYSIsInN1YiI6Im9lZWFwcHRlY2h1c2VyIiwic2NvcGUiOlsibWRzcDpjb3JlOkFkbWluM3JkUGFydHlUZWNoVXNlciJdLCJjbGllbnRfaWQiOiJvZWVhcHB0ZWNodXNlciIsImNpZCI6Im9lZWFwcHRlY2h1c2VyIiwiYXpwIjoib2VlYXBwdGVjaHVzZXIiLCJncmFudF90eXBlIjoiY2xpZW50X2NyZWRlbnRpYWxzIiwicmV2X3NpZyI6ImExNGMyZGUxIiwiaWF0IjoxNTY3NDkyOTMxLCJleHAiOjE1Njc0OTQ3MzEsImlzcyI6Imh0dHBzOi8vYXRvc2VkZS5waWFtLmV1MS5taW5kc3BoZXJlLmlvL29hdXRoL3Rva2VuIiwiemlkIjoiYXRvc2VkZSIsImF1ZCI6WyJvZWVhcHB0ZWNodXNlciJdLCJ0ZW4iOiJhdG9zZWRlIiwic2NoZW1hcyI6WyJ1cm46c2llbWVuczptaW5kc3BoZXJlOmlhbTp2MSJdLCJjYXQiOiJjbGllbnQtdG9rZW46djEifQ.Ltep-Jg9vCOQUNo2sA_0rBUPv4EK93OPwvYshtozh98d8vkYxrNVfd-_lA6kQnN3R7wTBnS-RW5VG9vtVTNcDT2DDU1S1vHVP-cJ22DgEjIW4FLed406EhaZoiwcwWWU6KFj0Yx69hGp_gdtHwngRHgEIcdAAWnTsOil-Er4rtSeajT6fFE_u7gsB1ubwZ85dsbyO3iEUfcj5aabccdnnVtC1UpYhm34Eus4V90dxIYBzF62VK7g5nplRwQRk3j2V45p-jAX_Itg1_wFwGcwzezyPLze5xphlLy2u9RcmpXd18f3r4e-qaQV-eTKyzza1EF3nV3Q0ZEmCl46V2FxBQ");
			connection.setRequestProperty("accept-encoding", "gzip, deflate");

			BufferedReader in = new BufferedReader(
					new InputStreamReader(connection.getInputStream()));
			String inputLine;
			StringBuilder response = new StringBuilder();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			return new ResponseEntity(response, HttpStatus.OK);  
		} catch (IOException e) {

			e.printStackTrace();
		}
		return new ResponseEntity(null, HttpStatus.SERVICE_UNAVAILABLE);
	}
	

	@RequestMapping(value = "/mindSphereNotificationAPI", method = RequestMethod.POST)
	public static void postRequest() throws IOException {
		System.out.println("Hello world....");

	    final String POST_PARAMS = "{\"body\":{\"message\":\"Rotor speed is accessed above the limt\"},\"recipientsTo\":\"akansha_bangde@syntelinc.com\",\"from\":\"Atos\",\"subject\":\"Notification\"}";
	    
	    System.out.println(POST_PARAMS);
		String url = "https://gateway.eu1.mindsphere.io/api/notification/v3/publisher/messages";
		Proxy proxy=null;
		URL urlObj;

			urlObj = new URL(url);
			 HttpURLConnection postConnection = (HttpURLConnection) urlObj.openConnection();
			    postConnection.setRequestMethod("POST");
			    postConnection.setRequestProperty("Content-Type", "application/json");
			    postConnection.setRequestProperty("Authorization", "Bearer "+"eyJhbGciOiJSUzI1NiIsImprdSI6Imh0dHBzOi8vYXRvc2VkZS5sb2NhbGhvc3Q6ODA4MC91YWEvdG9rZW5fa2V5cyIsImtpZCI6ImtleS1pZC0xIiwidHlwIjoiSldUIn0.eyJqdGkiOiIwMzU0ODg2MzhhOTM0MjVlYjVjZGFhYzM3ZWFlMWEyYSIsInN1YiI6Im9lZWFwcHRlY2h1c2VyIiwic2NvcGUiOlsibWRzcDpjb3JlOkFkbWluM3JkUGFydHlUZWNoVXNlciJdLCJjbGllbnRfaWQiOiJvZWVhcHB0ZWNodXNlciIsImNpZCI6Im9lZWFwcHRlY2h1c2VyIiwiYXpwIjoib2VlYXBwdGVjaHVzZXIiLCJncmFudF90eXBlIjoiY2xpZW50X2NyZWRlbnRpYWxzIiwicmV2X3NpZyI6ImExNGMyZGUxIiwiaWF0IjoxNTY3NDkyOTMxLCJleHAiOjE1Njc0OTQ3MzEsImlzcyI6Imh0dHBzOi8vYXRvc2VkZS5waWFtLmV1MS5taW5kc3BoZXJlLmlvL29hdXRoL3Rva2VuIiwiemlkIjoiYXRvc2VkZSIsImF1ZCI6WyJvZWVhcHB0ZWNodXNlciJdLCJ0ZW4iOiJhdG9zZWRlIiwic2NoZW1hcyI6WyJ1cm46c2llbWVuczptaW5kc3BoZXJlOmlhbTp2MSJdLCJjYXQiOiJjbGllbnQtdG9rZW46djEifQ.Ltep-Jg9vCOQUNo2sA_0rBUPv4EK93OPwvYshtozh98d8vkYxrNVfd-_lA6kQnN3R7wTBnS-RW5VG9vtVTNcDT2DDU1S1vHVP-cJ22DgEjIW4FLed406EhaZoiwcwWWU6KFj0Yx69hGp_gdtHwngRHgEIcdAAWnTsOil-Er4rtSeajT6fFE_u7gsB1ubwZ85dsbyO3iEUfcj5aabccdnnVtC1UpYhm34Eus4V90dxIYBzF62VK7g5nplRwQRk3j2V45p-jAX_Itg1_wFwGcwzezyPLze5xphlLy2u9RcmpXd18f3r4e-qaQV-eTKyzza1EF3nV3Q0ZEmCl46V2FxBQ");
			

			 postConnection.setDoOutput(true);
			    OutputStream os = postConnection.getOutputStream();
			    os.write(POST_PARAMS.getBytes());
			    os.flush();
			    os.close();
			    int responseCode = postConnection.getResponseCode();
			    System.out.println("POST Response Code :  " + responseCode);
			    System.out.println("POST Response Message : " + postConnection.getResponseMessage());
			    if (responseCode == HttpURLConnection.HTTP_CREATED) { //success
			        BufferedReader in = new BufferedReader(new InputStreamReader(
			            postConnection.getInputStream()));
			        String inputLine;
			        StringBuffer response = new StringBuffer();
			        while ((inputLine = in .readLine()) != null) {
			            response.append(inputLine);
			        } in .close();
			        // print result
			        System.out.println(response.toString());
			    } else {
			        System.out.println("POST NOT WORKED");
			    }
}
}